(require 'package)
(package-initialize)

;; Add new achives to the list
(defvar my-archives
  '(("melpa-stable" . "https://stable.melpa.org/packages/")
    ("melpa" . "https://melpa.org/packages/")))

(dolist (archive my-archives)
  (add-to-list 'package-archives archive t))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("11e57648ab04915568e558b77541d0e94e69d09c9c54c06075938b6abc0189d8" default)))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (multiple-cursors company doom-themes hlinum helm-projectile projectile flycheck helm magit git-gutter-fringe+ undo-tree molokai-theme csharp-mode omnisharp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; HINT : to automatically install selected package use the following command :
;; M-x package-install-selected-packages


;;;;;;;;;;;;;;;;;;;; UI

;; HINT note : next buffer is C-x right arrow (XF86Forward), previous is C-x left (XF86Back)


;; doom themes
(require 'doom-themes)

;; Global settings (defaults)
(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
      doom-themes-enable-italic t) ; if nil, italics is universally disabled

;; Load the theme (doom-one, doom-molokai, etc); keep in mind that each
;; theme may have their own settings.
;; (load-theme 'doom-tomorrow-night t)
(load-theme 'doom-molokai t)

;; Enable flashing mode-line on errors
(doom-themes-visual-bell-config)


(if (display-graphic-p)
    (progn
      ;; if graphic

      ;; highlight current line
      (global-hl-line-mode))

  ;; else (optional)
  (load "molokai-theme.el"))


;; highlight current line number in linum-mode
(require 'hlinum)
(hlinum-activate)


;; remove tool and menu bars
(tool-bar-mode -1)
(menu-bar-mode -1)


;; scroll one line at a time (less "jumpy" than defaults)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
;; (require 'smooth-scroll)
;; (smooth-scroll-mode t)


;;;;;;;;;;;;;;;;;;;;; GIT

(require 'git-gutter-fringe+)
(global-git-gutter+-mode)
(setq git-gutter-fr+-side 'right-fringe)


;;;;;;;;;;;;;;;;;;;;; CODE EDITION

(delete-selection-mode 1)
(electric-pair-mode 1)
(show-paren-mode 1)
(global-undo-tree-mode)

(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

;; remap C-a to `smarter-move-beginning-of-line'
(global-set-key [remap move-beginning-of-line]
                'smarter-move-beginning-of-line)


;; helm mode everywhere
(global-set-key (kbd "M-x") 'helm-M-x)
(helm-mode 1)


;; project management with projectile
(global-set-key (kbd "C-x M-f") 'helm-projectile-find-file-dwim)
(global-set-key (kbd "C-x M-g") 'helm-projectile-grep)


;; multiple cursors
(require 'multiple-cursors)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)


;; compile project with f9 key
(global-set-key (kbd "<f9>") 'compile)


(setq c-basic-offset 4)
(setq tab-width 4)
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)


;; Unity specifig features
(setq create-lockfiles nil) ;; disable lock files to avoid meta hell in Unity

;; C# specific features
(add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-mode))

(add-hook 'csharp-mode-hook 'omnisharp-mode)
(eval-after-load
 'company
 '(add-to-list 'company-backends 'company-omnisharp))
(add-hook 'csharp-mode-hook 'company-mode)
(add-hook 'csharp-mode-hook 'linum-mode)
(add-hook 'csharp-mode-hook 'flycheck-mode)
(add-hook 'csharp-mode-hook 'projectile-mode)

;; ;; Company completion specific
(setq company-idle-delay 0)
(global-set-key [(backtab)] 'company-complete)



(defun my-add-csharp-mode-bindings ()
  (local-set-key (kbd "C-c r r") 'omnisharp-run-code-action-refactoring)
  (local-set-key (kbd "<f12>") 'omnisharp-go-to-definition)
  (local-set-key (kbd "<C-f12>") 'omnisharp-go-to-definition-other-window)
  (local-set-key (kbd "<M-f12>") 'omnisharp-helm-find-usages)
;;  (local-set-key (kbd "C->") 'pop-tag-mark)
;;  (local-set-key (kbd "C-<") 'rtags-location-stack-forward)
  )

(add-hook 'csharp-mode-hook 'my-add-csharp-mode-bindings)


;; WIP
;; (add-hook 'haxe-mode-hook
;; 	  (lambda ()
;; 	    (set (make-local-variable \\='compile-command)
;; 		 (concat \"haxe compile.hxml \"
;; 			 (if buffer-file-name
;; 			     (shell-quote-argument
;; 			      (file-name-sans-extension buffer-file-name)))))))


;; in order to avoid conflicts with unity
(setq make-backup-files nil)

